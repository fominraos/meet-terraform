# Что нужно сделать

## 1 сервер
**Что нужно сделать**

- Зарегистрируйте аккаунт в AWS, активируйте [бесплатный доступ](https://aws.amazon.com/ru/free/activate-your-free-tier-account/) и получите 300 долларов на счёт. 
- Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) любой версии и [сконфигурируйте его](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) для управления аккаунтом и ресурсами. 
- Перейдите в [настройки](https://console.aws.amazon.com/ec2/), на панели навигации выберите Key Pairs и [загрузите свой SSH-ключ](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html), его имя будет использоваться в Terraform-скрипте.
- Скачайте и установите [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli).
- Возьмите код из репозитория `git clone https://gitlab.com/fominraos/meet-terraform.git`
- В файле `OneServer/main.tf` в блоке `resource "aws_instance" "my_webserver"` для параметра `key_name` добавьте название ключа из пункта 3.
- Перейдите в папку OneServer и инициализируйте ресурсы при помощи Terraform `terraform init`.
- Разверните инфраструктуру при помощи команды `terraform apply`.
- Дождитесь выполнения скрипта и в браузере и наберите полученный IP-адрес, через некоторое время там должно появиться наше ReactJS-приложение.
- Выполнить `terraform destoy` для удаления ресурсов

----

## 2 сервера
**Что нужно сделать**

- Разверните инфраструктуру с балансировкой нагрузки и двумя серверами при помощи Terraform из папки TwoServers, узнайте DNS-имя балансировщика.
- Попробуйте перейти в браузере по DNS-имени балансировщика.

